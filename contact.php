<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Contact Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Contact us </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>Contact</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">
               <div class="container">                   
                   <!-- row -->
                   <div class="row py-4">
                        <!-- col -->
                        <div class="col-lg-8">
                           <div class="row">
                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="addcol">
                                        <h6 class="h5">For General Enquries</h6>
                                        <p class="pb-0">Please mail at</p>
                                        <a href="mailto:info@capdt.com">info@capdt.com</a>
                                    </div>
                                </div>
                                <!--/ col -->

                                <!-- col -->
                                <div class="col-lg-6">
                                    <div class="addcol">
                                        <h6 class="h5">Career Related Enquiries</h6>
                                        <p class="pb-0">Please mail at</p>
                                        <a href="mailto:career@capdt.com">career@capdt.com</a>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-6">
                                    <div class="addcol">
                                        <h6 class="h5">For Contact directly</h6>
                                        <p class="pb-0">Please mail at</p>
                                        <a>+91 9642123254, +91 40 123456</a>
                                    </div>
                                </div>
                                <!--/ col -->

                                 <!-- col -->
                                 <div class="col-lg-6">
                                    <div class="addcol">
                                        <h6 class="h5">Address</h6>
                                        <p class="pb-0">Plot No:123, H.No: 22-4, Road No:1, Banjarahills, Hyderabad, Telangana, India - 002</p>                                        
                                    </div>
                                </div>
                                <!--/ col -->

                           </div>                           
                        </div>
                        <!-- col -->
                        <!-- col social -->
                        <div class="col-lg-4">
                            <table class="tablecontact">
                                <tr>
                                    <td><figure><a href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></figure></a></td>
                                    <td><p class="pb-0"><a href="javascript:void(0)" target="_blank">Like & Share on Facebook</a></p></td>
                                </tr>
                                <tr>
                                    <td><figure><a href="javascript:void(0)" target="_blank"><span class="icon-linkedin1 icomoon"></span></figure></a></td>
                                    <td><p class="pb-0"><a href="javascript:void(0)" target="_blank">Like & Share on Linkedin</a></p></td>
                                </tr>
                                <tr>
                                    <td><figure><a href="javascript:void(0)" target="_blank"><span class="icon-youtube-play icomoon"></span></figure></a></td>
                                    <td><p class="pb-0"><a href="javascript:void(0)" target="_blank">Watch on Youtube</a></p></td>
                                </tr>
                            </table>
                        </div>
                        <!--/ col social -->
                    </div>
                   <!--/ row --> 

                   <!-- row -->
                   <div class="row">
                       <div class="col-lg-6">
                           <p>Fill the following form for any queries, Our Executives will get back to you for proper response.</p>
                       </div>
                   </div>
                   <!--/ row -->

                   <form>
                        <!-- row -->                    
                        <div class="row">
                            <!-- col -->
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Phone Number</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <!--/ col -->

                             <!-- col -->
                             <div class="col-lg-4">
                                <div class="form-group">
                                    <label>Email Address</label>
                                    <input type="text" class="form-control">
                                </div>
                            </div>
                            <!--/ col -->
                        </div>
                        <!--/ row -->

                        <!-- row -->
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control w-100" style="height:100px;"></textarea>
                                </div>
                            </div>
                        </div>
                        <!--/ row -->
                        <input type="submit" value="Submit" class="btn">

                   </form>

               </div>
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>