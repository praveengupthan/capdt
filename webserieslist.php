<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Web Series </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>Web Series Title will be here</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">
               <div class="container">                   
                   <!-- row -->
                   <div class="row py-4">
                        <!-- col -->
                        <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries01.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries02.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries03.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries04.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries05.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries06.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries01.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries02.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries03.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries04.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries05.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="listitem webserieslist">
                                <figure>
                                    <a href="videolist.php"> <img src="img/data/webseries06.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a class="webanchor" href="videolist.php">Bachelor Season</a>
                                    <p class="features"><span>(25 Episodes)</span> <span class="float-right"><a href="javascript:void(0)">View All Episodes</a></span></p>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->
                    </div>
                   <!--/ row --> 
                   <!-- row -->
                   <div class="row pb-3">
                       <div class="col-lg-12">
                        <nav aria-label="Page navigation">
                                <ul class="pagination float-right">
                                    <li class="page-item"><a class="page-link" href="#">Previous</a></li>
                                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                                    <li class="page-item"><a class="page-link" href="#">4</a></li>
                                    <li class="page-item"><a class="page-link" href="#">5</a></li>
                                    <li class="page-item"><a class="page-link" href="#">6</a></li>
                                    <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                </ul>
                            </nav>
                       </div>
                   </div>
                   <!--/ row -->
               </div>
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>