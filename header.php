 <!--header -->
    <header class="fixed-top">
        <div class="search">
            <div class="searchin">
                <input type="text" placeholder="Search Keywords">
                <a href="javascript:void(0)"><span class="icon-close1 icomoon"></span></a>
            </div>                  
        </div>
        <div class="container">
            <nav id="topNav" class="navbar navbar-expand-lg px-0">
                <a class="navbar-brand mx-auto" href="index.php"><img src="img/logo.svg" title="" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                    Menu
                </button>
                <div class="navbar-collapse collapse">
                    <div class="dropdown">
                        <a href="javascript:void(0)"  id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <span class="icon-bars icomoon"></span> CHANNELS & STORIES </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="videolist.php">Oka Radha Iddaru Krishnulu</a>
                            <a class="dropdown-item" href="webserieslist.php">Capdt</a>
                            <a class="dropdown-item" href="videolist.php">Godavari Express</a>
                            <a class="dropdown-item" href="videolist.php">Hey Pilla</a> 
                            <a class="dropdown-item" href="articleslist.php">Diaries & Articles</a>                            
                        </div>
                    </div>
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item text-right">
                            <a class="nav-link border-right btn-search" href="javascript:void(0)" id="btnsearch" target="_blank"><span class="icon-search1 icomoon"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" target="_blank"><span class="icon-facebook icomoon"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" target="_blank"><span class="icon-linkedin1 icomoon"></span></a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="javascript:void(0)" target="_blank"><span class="icon-youtube-play icomoon"></span></a>
                        </li>
                    </ul>
                </div>           
            </nav>
        </div>
    </header>
    <!--/ header -->