<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> Web  Series of CAPDT </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main>
        <!-- slider section -->
        <section class="homecarouse">
            <div class="container-fluid ">
                <!-- row -->
                <div class="row">
                    <!-- col slider -->
                    <div class="col-lg-12 mx-0 px-0">
                        <!-- carousel-->
                        <div class="homecarouse">
                            <div class="carousel slide" data-ride="carousel">                               
                                <div class="carousel-inner" role="listbox">
                                    <!-- Slide One - Set the background image for this slide in the line below -->
                                    <div class="carousel-item active serieslist" style="background-image: url('img/data/seriesimg.jpg')">
                                    <a href="javascript:void(0)"><img class="svg" src="img/youtube.svg"></a>
                                        <div class="carousel-caption">
                                            <div class="captionin">
                                                <h5>Channel Name will be here</h5>
                                                <h2 class="slidertitle"><a href="articledetail.php">Here is Our latest Video Punugulu Success meet</a></h2>
                                                <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--/ carousel-->
                    </div>
                    <!--/ col slider -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!-- slider section -->  

        <!-- list of episodes in web series -->
        <section class="list-episodes">
            <!-- container -->
            <div class="container">
                 <!-- title -->
                <article class="hometitle py-4">
                    <h2>Series Name will be here <a href="javascript:void(0)">18 Series </a></h2>
                </article>
                <!-- title -->
                 <!-- row list item-->
                <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                        <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->

                 <!-- row list item-->
                 <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                        <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->

                 <!-- row list item-->
                 <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                        <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->

                 <!-- row list item-->
                 <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                    <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->

                 <!-- row list item-->
                 <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                        <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->

                 <!-- row list item-->
                 <div class="row py-3 my-3 seriesrow">
                    <!-- col -->
                    <div class="col-lg-3">
                        <figure class="seriesfig">
                            <a href="javascript:void(0)"><img src="img/data/article04.jpg" alt="" title="" class="img-fluid"></a>
                            <span class="time position-absolute">21.00</span>
                        </figure>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-6 descseries">
                        <h5><a href="javascript:void(0)">Episode Name will be here</a></h5>
                        <p>Welcome to the home of the Guptas, in the summer of '98. Meet Harshu, our 12-year old, naughty but nice hero. He has a strict but caring mother, a cool father who is the engine of the family, a nerdy elder brother and an adorable baby sister.</p>
                    </div>
                    <!--/ col -->
                    <!-- col -->
                    <div class="col-lg-3">
                        <table class="comtable">
                            <tr>
                                <td><img src="img/svg/comment.svg" class="svg"></td>
                                <td>25 Comments </td>
                            </tr>
                            <tr>
                                <td><img src="img/svg/thumbsup.svg" class="svg"></td>
                                <td>55 Likes </td>
                            </tr>
                        </table>
                    </div>
                    <!--/ col -->
                </div>
                <!--/ row  list item-->


            </div>
            <!--/ container -->
           
        </section>
        <!--/ list of episodes in web series -->
       
       
       



    </main>
    <!--/ main-->    
   <?php include 'footer.php'?>
   <?php include 'footerscripts.php'?>       
</body>
</html>