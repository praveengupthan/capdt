<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>About us</h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>About us</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">
               <div class="container">
                   <!-- row -->
                   <div class="row">
                       <div class="col-lg-6">
                           <img src="img/about01.jpg" class="img-fluid" alt="" title="">
                       </div>
                       <div class="col-lg-6 align-self-center">
                            <h2>Welcome to <span>Capdt Videos</span></h2>
                            <p>Capdt is one of the world’s largest internet-based TV and On-demand service provider for South Asian content, offering more than 250+ TV channels, 5000+ Movies and 100+ TV Shows in 14 languages. Capdt has 25000 hours of entertainment content catalogued in its library, while nearly 2500 hours of new on-demand content is added to the Capdt platform every day. Making the best use of technology growth and advancement, Yaits consumers to experience convenience of virtual home entertainment anytime, anywhere.</p>
                            <p>Through multiple screens - Connected TVs, Internet STBs, Smart Blu-ray Player, PCs, Smart Phones and Tablets.</p>
                       </div>
                   </div>
                   <!--/ row -->
                   <!-- row -->
                   <div class="row py-4">
                       <!-- col -->
                       <div class="col-lg-4">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 text-center ">
                                        <span class="icon-video-player icomoon"></span>
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <h3>Channels</h3>
                                        <p class="pb-0">18+</p>
                                    </div>
                                </div>
                            </div>
                       </div>
                       <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-4">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 text-center ">
                                        <span class="icon-video-player icomoon"></span>
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <h3>short films</h3>
                                        <p class="pb-0">300+</p>
                                    </div>
                                </div>
                            </div>
                       </div>
                       <!--/ col -->
                       <!-- col -->
                       <div class="col-lg-4">
                            <div class="highlate">
                                <div class="row ">
                                    <div class="col-lg-4 text-center ">
                                        <span class="icon-video-player icomoon"></span>
                                    </div>
                                    <div class="col-lg-8 align-self-center">
                                        <h3>Episodes</h3>
                                        <p class="pb-0">450</p>
                                    </div>
                                </div>
                            </div>
                       </div>
                       <!--/ col -->
                   </div>
                   <!--/ row -->
                   <!-- row -->
                   <div class="row">
                       <!-- col-->
                       <div class="col-lg-6">
                           <img src="img/about02.jpg" class="img-fluid">
                           <h2>Our <span>Vision</span></h2>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                       </div>
                       <!--/ col -->
                        <!-- col-->
                        <div class="col-lg-6">
                           <img src="img/about03.jpg" class="img-fluid">
                           <h2>Our Mission & <span>Core Values</span></h2>
                           <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
                       </div>
                       <!--/ col -->

                   </div>
                   <!--/ row -->
               </div>
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>