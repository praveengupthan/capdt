<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Funny Memes </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>Funny Memes</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">
               <div class="container">                   
                   <!-- row -->
                   <div class="row py-4">

                        <!-- col -->
                        <div class="col-lg-3">
                            <div class="pagecolumn">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article class="py-2">
                                    <div class="articlein"> 
                                        <a href="#" class="d-inline-block link video-btn" data-toggle="modal" data-src="img/data/memes01.jpg" data-target="#myModal">View More</a>
                                        <ul class="text-center">
                                            <li><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="pagecolumn">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article class="py-2">
                                    <div class="articlein"> 
                                        <a href="#" class="d-inline-block link video-btn" data-toggle="modal" data-src="img/data/memes03.jpg" data-target="#myModal">View More</a>
                                        <ul class="text-center">
                                            <li><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="pagecolumn">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article class="py-2">
                                    <div class="articlein"> 
                                        <a href="#" class="d-inline-block link video-btn" data-toggle="modal" data-src="img/data/memes02.jpg" data-target="#myModal">View More</a>
                                        <ul class="text-center">
                                            <li><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                         <!-- col -->
                         <div class="col-lg-3">
                            <div class="pagecolumn">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article class="py-2">
                                    <div class="articlein"> 
                                        <a href="#" class="d-inline-block link video-btn" data-toggle="modal" data-src="img/data/memes04.jpg" data-target="#myModal">View More</a>
                                        <ul class="text-center">
                                            <li><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                            <li><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                        </div>
                        <!--/ col -->

                   </div>
                   <!--/ row -->                  
               </div>
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>   
   
   
<!-- popup video modal -->

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog videodialog" role="document">
    <div class="modal-content">      
      <div class="modal-body">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>        
        <!-- 16:9 aspect ratio -->
        <div class="embed-responsive embed-responsive-16by9">
            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always">></iframe>
        </div> 
      </div>
    </div>
  </div>
</div> 
<!--/ popup video modal -->
   
  
</body>
</html>