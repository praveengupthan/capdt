<!-- style sheets -->
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/html-5-reset-css.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/resCarousel.css">
<link rel="stylesheet" href="css/fonts.css">

<link rel="icon" type="image/png" sizes="32x32" href="img/fav.png">

