<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Video detail </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
    <!-- comment iframe -->
    <div id="fb-root"></div>
<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v3.2&appId=361437837778249&autoLogAppEvents=1"></script> 
    <!--/ comment i frame -->
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Video Name will be here </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="articleslist.php">Videos</a></li>
                                <li><a>Video Name will be here</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">                
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                       <!-- left article col -->
                       <div class="col-lg-12">
                           <div class="articledetail">
                               <h4 class="h5 py-3 border-bottom">First single from Gopichand’s Pantham to release tomorrow</h4>
                               <!-- row -->
                               <div class="row">
                                   <!-- col-4 -->
                                   <div class="col-lg-4">
                                        <p class="themecolor dtnote"><span>14-06-2018</span><span>Press Note</span></p>
                                   </div>
                                   <!-- col-4 -->
                                   <!-- col-8-->
                                   <div class="col-lg-8 text-right">
                                        <div class="detailsocial float-right">
                                            <p class="float-left pr-2">Recommend to your friends</p>
                                            <ul class="float-left nav">
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialutube.jpg"></a></li>
                                            </ul>
                                            
                                        </div>
                                   </div>
                                   <!--/ col-8-->
                               </div>
                               <!--/ row -->
                               <!-- row -->
                               <div class="row">
                                   <div class="col-lg-12">                                      
                                       <iframe width="100%" height="550" src="https://www.youtube.com/embed/BJRJwEFBQ-k" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>    
                                        
                                      <div class="row">
                                          <div class="col-lg-12">
                                                <div class="captionin">
                                                    <h5>Channel Name will be here</h5>
                                                    <h2 class="slidertitle py-2"><a href="articledetail.php">Here is Our latest Video Punugulu Success meet</a></h2>
                                                    <p>Sahil has a Surprise for Surbhi and she doesn’t seem to like it very much! Will Sahil’s dream of becoming a power couple the vegan way work out or is Surbhi going to get her way and win the chicken dinner? Watch Main Vegan Banna Chahta Hoon and find out!</p>
                                                </div>
                                          </div>
                                      </div>                                     
                                       
                                   </div>
                               </div>
                               <!--/row -->
                               <!-- row -->
                               <div class="row">
                                   <div class="col-lg-12">
                                        <!-- comment box -->
                                        <div class="fb-comment-embed" 
data-href="https://www.facebook.com/zuck/posts/10102577175875681?comment_id=1193531464007751&amp;reply_comment_id=654912701278942" data-width="560" data-include-parent="false"></div> 
                                        <!--/ comment box -->
                                   </div>
                               </div>
                               <!--/ row -->
                               
                           </div>
                       </div>
                       <!--/ left article col -->                       
                    </div>
                    <!--/ row -->                   
                </div>
                <!--/ container -->    
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>