<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Privacy Policy </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>Privacy Policy</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">
               <div class="container">                   
                   <!-- row -->
                   <div class="row py-4">
                        <div class="col-lg-12">
                            <h2 class="subtitle">1. Description of Service and Acceptance of Terms</h2>
                            <p>Contagious Online Media Network Private Limited (TVF) ("TVF," "we," or "us") provides an online video service offering a selection of shows, movies, clips, and other content (collectively, the "Content"). Our video service, the Content, our player for viewing the Content (the "Video Player") and any other features, tools, materials, or other services (including third party branded services) offered from time to time by TVF through a variety of Access Points (defined below) are referred to collectively as the "Services." The term "Access Points" refers to, collectively, the http://capdt.com/ website (the "capdt"), applications, and other places where any Services are available, including websites and applications of TVF's third party distribution partners and other websites where users or website operators are permitted to embed or have otherwise licensed the Video Player.
                            </p>
                            <p>Use of the Services (including access to the Content) is subject to compliance with these Terms and any end user license agreement that might accompany the applicable Service. Therefore, by visiting the TVF Site or using any of the Services through any other Access Point you are agreeing to these Terms.</p>

                            <h2 class="subtitle">2. Changes to These Terms</h2>
                            <p>The Terms is subject to revisions at any time, as determined by TVF, without notice, and any such changes are effective immediately upon being posted on the Website; any use of the Website thereafter will be deemed to be an acceptance of these changes by the User. Users are strongly urged to read these Terms in its entirety and to periodically check this page to understand how modifications or revisions to this policy affect the use of their information. TVF shall not be responsible for any User's failure to remain informed about such changes.</p>

                            <h2 class="subtitle">3. Access and Use of the Services</h2>
                            <p>By visiting the Site or using any of the Services, You accept and agree to be bound by these Terms and Conditions and the Privacy Policy and to abide by all applicable laws, rules and regulations. Unless otherwise specified, the Services are available for individuals aged 18 years or older. If You are under the age of 18, You should review these Terms and Conditions and the Privacy Policy with Your parent or legal guardian to make sure that You and Your parent or legal guardian understands and agrees to it on Your behalf and further if required, You shall perform or undertake such activities which will entitle You to enter into a legally binding agreement with TVF.</p>
                            <p>TVF grants You a non-exclusive limited, revocable, limited permission to use the Services on the Site for personal, non-commercial purposes during the subsistence of Your Account for the territory of the world or limited territories as applicable in respect of specified Content and as set forth in these Terms and Conditions.</p>
                            <p>You may only access and view the Content personally and for a non-commercial purpose in compliance with these Terms. You may not either directly or through the use of any device, software, internet site, web-based service, or other means remove, alter, bypass, avoid, interfere with, or circumvent any copyright, trademark, or other proprietary notices marked on the Content or any digital rights management mechanism, device, or other content protection or access control measure associated with the Content including geo-filtering mechanisms. You may not either directly or through the use of any device, software, internet site, web-based service, or other means copy, download, stream capture, reproduce, duplicate, archive, distribute, upload, publish, modify, translate, broadcast, perform, display, sell, transmit or retransmit the Content unless expressly permitted by TVF in writing. You may not incorporate the Content into, or stream or retransmit the Content via, any hardware or software application or make it available via frames or in-line links unless expressly permitted by TVF in writing. Furthermore, you may not create, recreate, distribute or advertise an index of any significant portion of the Content unless authorized by TVF. You may not build a business utilizing the Content, whether or not for profit. The Content covered by these restrictions includes without limitation any text, graphics, layout, interface, logos, photographs, audio and video materials, and stills. In addition, you are strictly prohibited from creating derivative works or materials that otherwise are derived from or based on in any way the Content, including montages, mash-ups and similar videos, wallpaper, desktop themes, greeting cards, and merchandise, unless it is expressly permitted by TVF in writing. This prohibition applies even if you intend to give away the derivative materials free of charge.</p>


                        </div>
                    </div>
                   <!--/ row --> 
               </div>
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>