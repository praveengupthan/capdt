<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main>
        <!-- slider section -->
        <section class="homecarouse">
            <div class="container-fluid ">
                <!-- row -->
                <div class="row">
                    <!-- col slider -->
                    <div class="col-lg-12 mx-0 px-0">
                        <!-- carousel-->
                        <div class="homecarouse">
                            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner" role="listbox">
                                    <!-- Slide One - Set the background image for this slide in the line below -->
                                    <div class="carousel-item active serieslist" style="background-image: url('img/slider01.jpg')">
                                         <a href="javascript:void(0)"><img class="svg" src="img/youtube.svg"></a>
                                        <div class="carousel-caption d-none d-md-block">
                                            <div class="captionin">
                                                <h5>Channel Name will be here</h5>
                                                <h2 class="slidertitle"><a href="articledetail.php">Here is Our latest Video Punugulu Success meet</a></h2>
                                                <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Slide Two - Set the background image for this slide in the line below -->
                                    <div class="carousel-item serieslist" style="background-image: url('img/slider02.jpg')">
                                        <a href="javascript:void(0)"><img class="svg" src="img/youtube.svg"></a>
                                        <div class="carousel-caption d-none d-md-block">
                                            <div class="captionin">
                                                <h5>Channel Name will be here</h5>
                                                <h2 class="slidertitle"><a href="articledetail.php">Here is Our latest Video Punugulu Success meet</a></h2>
                                                <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Slide Three - Set the background image for this slide in the line below -->
                                    <div class="carousel-item serieslist" style="background-image: url('img/slider03.jpg')">
                                        <a href="javascript:void(0)"><img class="svg" src="img/youtube.svg"></a>
                                        <div class="carousel-caption d-none d-md-block">
                                            <div class="captionin">
                                                <h5>Channel Name will be here</h5>
                                                <h2 class="slidertitle"><a href="videodetail.php">Here is Our latest Video Punugulu Success meet</a></h2>
                                                <p>In publishing and graphic design, lorem ipsum is a placeholder text commonly used to demonstrate the visual form of a document without relying on meaningful content</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                                    <span class="icon-chevron-left icomoon"></span>
                                </a>
                                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                                    <span class="icon-chevron-right icomoon"></span>
                                </a>
                            </div>
                        </div>
                        <!--/ carousel-->
                    </div>
                    <!--/ col slider -->
                </div>
                <!--/ row -->
            </div>
        </section>
        <!-- slider section -->  
        <!-- channel section -->      
        <section class="channels py-3">
            <div class="container">
                <article class="hometitle py-4 text-center">
                    <h2>Our Entertainment Channels </h2>
                </article>
                <!-- row -->
                <div class="row justify-content-center">

                    <!-- col -->
                    <div class="col-lg-3 text-center channelcol">
                        <figure>
                            <a href="videolist.php"><img src="img/capdtchannel01.jpg" alt="" title=""></a>
                        </figure>
                        <article>
                            <h5 class="text-uppercase"><a href="videolist.php">Capdt</a></h5>
                            <p>140,046 subscribers</p>
                        </article>
                    </div>
                    <!--/ col -->

                     <!-- col -->
                     <div class="col-lg-3 text-center channelcol">
                        <figure>
                            <a href="videolist.php"><img src="img/capdtchannel02.jpg" alt="" title=""></a>
                        </figure>
                        <article>
                            <h5 class="text-uppercase"><a href="videolist.php">Godavari Express</a></h5>
                            <p>17,846 subscribers</p>
                        </article>
                    </div>
                    <!--/ col -->

                    <!-- col -->
                    <div class="col-lg-3 text-center channelcol">
                        <figure>
                            <a href="videolist.php"><img src="img/capdtchannel03.jpg" alt="" title=""></a>
                        </figure>
                        <article>
                            <h5 class="text-uppercase"><a href="videolist.php">Hey Pilla!</a></h5>
                            <p>30,125 subscribers</p>
                        </article>
                     </div>
                    <!--/ col -->

                </div>
                <!--/row -->
            </div>
        </section>
        <!-- channel section -->
        <!-- popular web series -->
        <section class="homewebseries">
            <div class="container">
                <article class="hometitle py-4">
                    <h2>Popular Web Series <a href="serieslistpage.php">View All</a></h2>
                </article>
            </div>
            <!-- slider -->
            <section class="sliderhome">
                <div class="container-fluid">
                    <div class="row">
                        <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900" data-animator="lazy">
                            <div class="resCarousel-inner">
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries01.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries02.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries03.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                                <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries04.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries05.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries06.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries01.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries02.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries03.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="jvideolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                <figure>
                                    <img src="img/data/webseries04.jpg" alt="" title="" class="img-fluid w-100">
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="videolist.php">View All Videos</a></h5>
                                </article>
                            </div>
                            <!--/ item-->
                        </div>
                            <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                            <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- slider -->
        </section>
        <!--/ popular web series -->
        <!-- latest entertainment videos -->
        <section class="videosrow">
            <div class="container">
                <article class="hometitle py-4">
                    <h2>Latest Entertainment Videos <a href="videolist.php">View All</a></h2>
                </article>
            </div>
            <!--slider -->
            <section class="videoslist">
                <div class="container-fluid">
                    <div class="row">
                        <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900" data-animator="lazy">
                            <div class="resCarousel-inner">
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                 <!--item -->
                                 <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                            </div>
                                <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                        </div>
                    </div>
                </div>                    
            </section>
            <!--/slider-->
        </section>
        <!--/ latest entertainment videos -->

         <!-- Most Popular  videos -->
         <section class="videosrow">
            <div class="container">
                <article class="hometitle py-4">
                    <h2>Latest Entertainment Videos <a href="videolist.php">View All</a></h2>
                </article>
            </div>
            <!--slider -->
            <section class="videoslist">
                <div class="container-fluid">
                    <div class="row">
                        <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900" data-animator="lazy">
                            <div class="resCarousel-inner">
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                    <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/latest01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>                                       
                                        <h5><a href="articledetail.php">Dummy text will be here with completed basic description</a></h5>
                                    </article>
                                </div>
                                <!--/ item-->
                            </div>
                                <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                        </div>
                    </div>
                </div>                    
            </section>
            <!--/slider-->
        </section>
        <!--/ Most Popular Videos videos -->

        <!-- Capdt Diaries & Articles -->
        <section class="diaries">
            <div class="container">
                <article class="hometitle py-4">
                    <h2>Capdt Diaries & Articles <a href="videolist.php">View All</a></h2>
                </article>
            </div>
            <!--slider -->
            <section class="videoslist">
                <div class="container-fluid">
                    <div class="row">
                        <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900" data-animator="lazy">
                            <div class="resCarousel-inner">
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article06.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article06.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                                    <!--item -->
                                    <div class="item col-lg-2">
                                    <figure>
                                        <a href="articledetail.php"><img src="img/data/article03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                    </figure>
                                    <article>    
                                        <h5 class="h5"><a href="articledetail.php">Title Name will be here</a></h5> 
                                        <h5>Dummy text will be here with completed basic description</h5>
                                    </article>
                                </div>
                                <!--/ item-->
                            </div>
                                <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                                <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                        </div>
                    </div>
                </div>                    
            </section>
            <!--/slider-->
        </section>
        <!--/ Capdt Diaries & Articles -->

         <!-- Funny Memes -->
         <section class="memes">
            <div class="container">
                <article class="hometitle py-4">
                    <h2>Funny Memes <a href="memeslist.php">View All</a></h2>
                </article>
            </div>
            <!-- slider -->
            <section class="sliderhome">
                <div class="container-fluid">
                    <div class="row">
                        <div class="resCarousel" data-items="2-3-3-3" data-slide="1" data-speed="900" data-animator="lazy">
                            <div class="resCarousel-inner">

                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                             
                            </div>
                            <!--/ item-->

                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                        <a href="javascript:void(0)"><img src="img/data/memes02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure> 
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->

                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                        <a href="javascript:void(0)"><img src="img/data/memes03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                              
                            </div>
                            <!--/ item-->
                                <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                               
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes01.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes02.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes03.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                            <!--item -->
                            <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes04.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure> 
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                                <!--item -->
                                <div class="item col-lg-2">
                                <figure>
                                    <a href="javascript:void(0)"><img src="img/data/memes05.jpg" alt="" title="" class="img-fluid w-100"></a>
                                </figure>
                                <article>
                                    <h4>Bachelor Season</h4>
                                    <h5><a href="javascript:void(0)">View More</a></h5>
                                </article>                                
                            </div>
                            <!--/ item-->
                        </div>
                            <a class='leftRs'><span class="icon-chevron-left icomoon"></span></a>
                            <a class='rightRs'><span class="icon-chevron-right icomoon"></span></a>
                        </div>
                    </div>
                </div>
            </section>
            <!-- slider -->
        </section>
        <!--/ Funny Memes -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>