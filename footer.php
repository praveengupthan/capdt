 <!--footer-->
    <footer class="pt-4">
        <div class="container">
            <!-- row -->
            <div class="row">
                <!-- col -->
                <div class="col-lg-5">
                    <div class="signcol">
                        <h5>Sign up for News Letter</h5>
                        <p class="py-3">Join our Adventure Tribe to get the latest travel tips,  stories, inspiration and a FREE adventure e-book.</p>
                        <div class="row">
                            <div class="col-lg-8 pr-0">
                                <input type="text" placeholder="Enter Your Email" class="form-control">
                            </div>
                            <div class="col-lg-4 pl-0">
                                <input type="submit" class="btn" value="Subscribe">
                            </div>
                        </div>
                    </div>
                </div>
                <!--/ col -->
                <!-- col -->
                <div class="col-lg-7 text-right align-self-center">
                    <ul class="nav float-right">
                        <li class="nav-item"><a href="index.php" class="nav-link">Home</a></li>
                        <li class="nav-item"><a href="about.php" class="nav-link">About us</a></li>
                        <li class="nav-item"><a href="career.php" class="nav-link">Career</a></li>
                        <li class="nav-item"><a href="terms.php" class="nav-link">Terms </a></li>
                        <li class="nav-item"><a href="privacy.php" class="nav-link">Privacy Policy</a></li>
                        <li class="nav-item"><a href="contact.php" class="nav-link">Contact</a></li>
                    </ul>
                </div>
                <!--/ col -->
            </div>
            <!--/ row -->
        </div> 
        <!-- copy rights -->
        <section class="copyrights mt-3">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-6 text-center">
                        <p>All copy rights reserved @capdtvideos2019</p>
                    </div>
                </div>
            </div>
        </section>
        <!--/ copy rights -->
    </footer>
    <!--/ footer -->