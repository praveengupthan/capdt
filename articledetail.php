<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Article Detail </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Capdt Diaries & Articles </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="articleslist.php">Article</a></li>
                                <li><a>Article Detail Title Name will be here</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">                
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                       <!-- left article col -->
                       <div class="col-lg-9">
                           <div class="articledetail">
                               <h4 class="h5 py-3 border-bottom">First single from Gopichand’s Pantham to release tomorrow</h4>
                               <!-- row -->
                               <div class="row">
                                   <!-- col-4 -->
                                   <div class="col-lg-4">
                                        <p class="themecolor dtnote"><span>14-06-2018</span><span>Press Note</span></p>
                                   </div>
                                   <!-- col-4 -->
                                   <!-- col-8-->
                                   <div class="col-lg-8 text-right">
                                        <div class="detailsocial float-right">
                                            <p class="float-left pr-2">Recommend to your friends</p>
                                            <ul class="float-left nav">
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialfb.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialgplus.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialtwitter.jpg"></a></li>
                                                <li class="nav-item"><a href="javascript:void(0)"><img src="img/socialutube.jpg"></a></li>
                                            </ul>
                                            
                                        </div>
                                   </div>
                                   <!--/ col-8-->
                               </div>
                               <!--/ row -->
                               <!-- row -->
                               <div class="row">
                                   <div class="col-lg-12">
                                       <figure class="pb-2">
                                           <img src="img/data/detailimg.jpg" class="img-fluid w-100">
                                       </figure>
                                   </div>
                               </div>
                               <!--/row -->
                               <!-- row -->
                               <div class="row">
                                   <div class="col-lg-12">
                                       <p>Actor Gopichand’s most awaited film Pantham, that has the tagline ‘For a Cause’, producer by K.K. Radhamohan on his Sri Satya Sai Arts banner is set to release on July 5. And the team recently wrapped up shoot and is now busy with the film’s post production. Meanwhile, the film’s promotional activities have begun too.</p>

                                       <p>The team is all set to release the first single from the film, First Time on Friday at 12.15 pm. Gopi Sunder is said to have come up with some very fresh tunes for the flick. Directed by K. Chakravarthy, who wrote the screenplay for films like Balupu, Power and Jai Lava Kusa, the film is Gopichand’s landmark 25th film, and the producers have gone all out to ensure it will be special for him. Actor Gopichand’s most awaited film Pantham, that has the tagline ‘For a Cause’, producer by K.K. Radhamohan on his Sri Satya Sai Arts banner is set to release on July 5. And the team recently wrapped up shoot and is now busy with the film’s post production. Meanwhile, the film’s promotional activities have begun too.</p>

                                       <p>The team is all set to release the first single from the film, First Time on Friday at 12.15 pm. Gopi Sunder is said to have come up with some very fresh tunes for the flick. Directed by K. Chakravarthy, who wrote the screenplay for films like Balupu, Power and Jai Lava Kusa, the film is Gopichand’s landmark 25th film, and the producers have gone all out to ensure it will be special for him.</p>

                                       <p>Production values of the film are just about okay. Music by Chirantan Bhatt is decent as all the songs sound good on screen. But the background score could have been a bit better during the crunch elevation scenes. The camerawork is okay and showcases the Kumbakonam region well. Dialogues written for Balayya are quite massy and will appeal to the front benchers. Editing is pretty average as the film needs serious cuts to make things better.Coming to the director Ravi Kumar, he has done just an okay job with the film. Even though he has presented Balakrishna quite well, his narration is quite dull in major places. The way he adds unnecessary commercial elements and sidetracks the film frequently deviates the attention of the audience most of the time.</p>

                                       <p>On the whole, Jai Simha is a typical mass masala entertainer in true Balakrishna style. The added advantage of the film is the emotional angle during the climax. The film is clearly aimed at the masses and will do well with all Balakrishna fans. But the roller coaster effect of the film can get to the regular audience at times. If you are the one who does not mind the regular commercial narration, this film ends up as a one time watch this festival season.</p>
                                       
                                   </div>
                               </div>
                               <!--/ row -->
                               
                           </div>
                       </div>
                       <!--/ left article col --> 
                       <!-- right article col -->
                       <div class="col-lg-3">
                            <h2>Related <span>News</span></h2>

                            <div class="listitem">
                                <figure>
                                    <a href="articledetail.php"> <img src="img/data/article01.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a href="articledetail.php">Barely Speaking with Arnub | azuddin Siddiqui</a>
                                </article>
                            </div>

                            <div class="listitem">
                                <figure>
                                    <a href="articledetail.php"> <img src="img/data/article02.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a href="articledetail.php">Barely Speaking with Arnub | azuddin Siddiqui</a>
                                </article>
                            </div>

                            <div class="listitem">
                                <figure>
                                    <a href="articledetail.php"> <img src="img/data/article03.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a href="articledetail.php">Barely Speaking with Arnub | azuddin Siddiqui</a>
                                </article>
                            </div>

                            <div class="listitem">
                                <figure>
                                    <a href="articledetail.php"> <img src="img/data/article04.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a href="articledetail.php">Barely Speaking with Arnub | azuddin Siddiqui</a>
                                </article>
                            </div>

                            <div class="listitem">
                                <figure>
                                    <a href="articledetail.php"> <img src="img/data/article05.jpg" alt="" title="" class="img-fluid"> </a>
                                </figure>
                                <article>
                                    <a href="articledetail.php">Barely Speaking with Arnub | azuddin Siddiqui</a>
                                </article>
                            </div>

                       </div>
                       <!--/ right article col -->
                       
                    </div>
                    <!--/ row -->                   
                </div>
                <!--/ container -->    
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>