<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Career Capdt Videos Entertainment Youtube Channel </title>
    <?php include 'headerstyles.php' ?>
    
</head>
<body>
   <?php include 'header.php' ?>
    <!--main -->
    <main class="subpagemain">
       <!-- sub page -->
       <section class="subpage">
           <!-- sub page header -->
           <section class="subpage-header">
                <div class="container">
                    <div class="row">
                        <!-- col -->
                        <div class="col-lg-4">
                            <article class="pagetitle">
                                <h1>Career </h1>
                                <p> Lorem Ipsum is simply dummy text of the printing and   typesetting industry.</p>
                            </article>
                        </div>
                        <!--/ col -->
                        <!-- col -->
                        <div class="col-lg-8 text-right align-self-end">
                            <ul class="nav brcrumb float-right">
                                <li><a href="index.php">Home</a></li>
                                <li><a>Career</a></li>
                            </ul>
                        </div>
                        <!--/ col -->
                    </div>
                </div>
           </section>
           <!--/ sub page header -->
           <!-- sub page body -->
           <section class="subpagebody">                
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <div class="col-lg-6">
                            <img src="img/career01.jpg" class="img-fluid">
                        </div>
                        <div class='col-lg-6 align-self-center'>
                            <h2>Join with <span>Capdt</span></h2>
                            <p>As a kid did you get warned for watching TV, yet you couldn’t resist it? Did people run away from you, for you tampered every electronic device at hand out of curiosity? Do you consume internet data more than food? If yes, then Capdt is a platform that will feed your curiosity that will ignite new ideas, is a workplace where you don’t work but watch TV 24*7.</p>
                            <p>As a kid did you get warned for watching TV, yet you couldn’t resist it? Did people run away from you, for you tampered every electronic device at hand out of curiosity? Do you consume internet data more than food? If yes, then Capdt is a platform that will feed your curiosity that will ignite new ideas, is a workplace where you don’t work but watch TV 24*7. </p>
                        </div>
                    </div>
                    <!--/ row -->
                    <!-- row -->
                    <div class="row justify-content-center py-5">
                        <!-- col -->
                        <div class="col-lg-6 text-center">
                            <h2>Job Openings at <span>Capdt</span></h2>
                            <p style="text-align:center;">Drop us a mail at <a class="themecolor" href="mailto:career@capdt.com"> career@capdt.com</a> </p>

                            <p style="text-align:center;">We'll be happy to hear from you.   Alternatively, choose from our list of challenges. And apply for the one you're ready to take head on.</p>
                        </div>
                        <!--/ col -->
                    </div>
                    <!--/ row -->
                </div>
                <!--/ container -->    
           </section>
           <!--/ sub page body -->
       </section>
       <!--/ sub page -->
    </main>
    <!--/ main-->    
   <?php include 'footer.php' ?>
   <?php include 'footerscripts.php' ?>       
</body>
</html>